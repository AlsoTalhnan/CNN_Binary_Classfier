# CNN Binary Classifier

## Presenation
This project is a simple Convolutional Neural Network used to classify images. The goal is to be able to distinguish 
between two types of images. In this example we are using memes: Thinking emoji and Pepe the frog.

| ![example of a pepe](prepared_pepe/128_prepared.png) | ![example of thinking](prepared_thinking/58_prepared.png) |
|:--:|:--:|
| *Pepe the frog* | *Thinking emoji* |

This project uses the library Keras (with the Tensorflow backend) to build a really classical CNN model:

Conv2D -> Relu -> Pooling -> Conv2D -> Relu -> Pooling -> Conv2D -> Relu -> Pooling -> Fully Connected

## Results

The model as a 92% accuracy on the validation dataset with the choosen hyperparamters. The dataset uses only ~300images of each. We could probably obtain better results by aquiring more data and tweaking hyperparameters a bit.This is only a simple project and does not aim to have a really great accuracy as long as it works. Using an otheroptimizer might lead to better result.# The datasetThe dataset is already pre-processed, all images have been squared to 128x128 pixels for convinience.This pre-processing have been done using the squareup script [Fred's imagemagick scripts](http://www.fmwconcepts.com/imagemagick/squareup/index.php)

![evolution of the model](acc_loss.png)
